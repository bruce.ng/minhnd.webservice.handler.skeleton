package handler.endpoint;

import handler.ws.ServerInfo;

import javax.xml.ws.Endpoint;

/**Endpoint publisher*/
public class WsPublisher {
	
	public static void main(String[] args) {
		
		Endpoint.publish("http://localhost:9999/ws/handler/server", new ServerInfo());
		System.out.println("START FINISH.....Service is published!");
		
	}
}
